import LocalizedStrings from 'react-localization';

export const searchTypes = new LocalizedStrings({
    en: {
        people: "People",
        movies: "Movies",
        companies: "Companies",
        tv: "TV Shows",
        collections: "Collections",
        keywords: "Keywords"
    }
});

export const search = new LocalizedStrings({
    en:{
        searchPlaceholder: "Your query here",
        searchButton: "GO"
    }
});

export const authentication = new LocalizedStrings({
    en:{
        authenticateButton: "Authenticate",
        authenticationError: "We could not authenticate you using this api key. Are you sure it is valid?"
    }
});
