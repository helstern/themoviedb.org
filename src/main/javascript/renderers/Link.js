import React from "react";

/**
 * @param {{title, id}} data
 * @returns {*}
 */
export function renderMovie(data)
{
  const { title, id } = data;

  return (<a target={'blank'} href={`https://www.themoviedb.org/movie/${id}`}>{title}</a>);
}

/**
 * @param {{name, id}} data
 * @returns {*}
 */
export function renderKeyword(data)
{
  const { name, id } = data;

  return (<a target={'blank'} href={`https://www.themoviedb.org/keyword/${id}`}>{name}</a>);
}

/**
 * @param {{name, id}} data
 * @returns {*}
 */
export function renderCompany(data)
{
  const { name, id } = data;

  return (<a target={'blank'} href={`https://www.themoviedb.org/company/${id}`}>{name}</a>);
}

/**
 * @param {{name, id}} data
 * @returns {*}
 */
export function renderCollection(data)
{
  const { name, id } = data;

  return (<a target={'blank'} href={`https://www.themoviedb.org/collection/${id}`}>{name}</a>);
}
