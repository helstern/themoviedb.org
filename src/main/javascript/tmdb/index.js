import createMovies from './movies/endpoints'
import createCollections from './collections/endpoints'
import createCompanies from './companies/endpoints'
import createKeywords from './keywords/endpoints'
import createPeople from './people/endpoints'
import createTv from './tv/endpoints'
import createSearch from './search/endpoints'
import createConfiguration from './configuration/endpoints'

export { default as createClient } from './client'

/**
 * @typedef {object} API
 * @property {ResourceMovies} movies
 * @property {ResourceCollections} collections
 * @property {ResourceCompanies} companies
 * @property {ResourceKeywords} keywords
 * @property {ResourcePeople} people
 * @property {ResourceTV} tv
 * @property {ResourceSearch} search
 * @property {ResourceConfiguration} configuration
 */

/**
 * @param {function} client
 * @returns {API}
 */
export function createApi(client)
{
    return {
        movies:         createMovies(client),
        collections:    createCollections(client),
        companies:      createCompanies(client),
        keywords:       createKeywords(client),
        people:         createPeople(client),
        tv:             createTv(client),
        search:         createSearch(client),
        configuration:  createConfiguration(client),
    }
}