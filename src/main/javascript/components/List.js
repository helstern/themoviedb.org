import React from "react";
import PropTypes from 'prop-types';
import classNames from "classnames";

export default class List extends React.PureComponent
{
  static propTypes = {
    items       : PropTypes.array.isRequired,
    renderItem  : PropTypes.func.isRequired,
    className   : PropTypes.string,
    listStyle   : PropTypes.oneOf(['comma-separated', 'bullet-list', 'ruled-notebook'])
  };

  static defaultProps = {
    className: "",
  };

  render()
  {
    return (
      <ul className={classNames("list", this.props.className, this.props.listStyle)}>
        {this.props.items.map(item => <li>{this.props.renderItem(item)}</li>)}
      </ul>
    );
  }
}