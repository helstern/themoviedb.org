import React from "react";
import PropTypes from 'prop-types';

class PageHome extends React.PureComponent
{
  static propTypes = {
    renderSearchBox:      PropTypes.func.isRequired,
  };

  render()
  {
    return(
      <section className={"section"}>
        <div className={"container"}>

          <h1 className={"title"}>Type something into the box and hit Enter</h1>

          {this.props.renderSearchBox()}
        </div>
      </section>
    );

  }
}

export default PageHome;