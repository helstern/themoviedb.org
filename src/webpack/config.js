const path = require('path');
const fs = require('fs');

const PROJECT_ROOT_PATH = path.resolve(__dirname, '../../');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    mode: 'development',

    entry: {
        main: [
            `webpack-dev-server/client?http://localhost:31080`,
            path.resolve(PROJECT_ROOT_PATH, 'src/webpack/entrypoint.js')
        ]
        // 'install-vendor' bundle is create by CommonsChunkPlugin
    },

    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM',
    },

    output: {
        chunkFilename: 'assets/[name].js',
        filename: '[name].js',
        path: path.resolve(PROJECT_ROOT_PATH, 'target'),
        publicPath: `/assets/`
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                include: [
                    path.resolve(PROJECT_ROOT_PATH, 'src/main/javascript'),
                ]
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
            { test: /\.(png|jpg)$/, use: 'url-loader?limit=15000' },
            { test: /\.eot(\?v=\d+.\d+.\d+)?$/, use: 'file-loader' },
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: 'url-loader?limit=10000&mimetype=application/font-woff' },
            { test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/, use: 'url-loader?limit=10000&mimetype=application/octet-stream' },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: 'url-loader?limit=10000&mimetype=image/svg+xml' }
        ],
    },

};
