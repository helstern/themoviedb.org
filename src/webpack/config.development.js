const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');

const PROJECT_ROOT_PATH = path.resolve(__dirname, '../../');

module.exports = {
    mode: 'development',

    entry: {
        main: [
            `webpack-dev-server/client?http://localhost:31080`,
            path.resolve(PROJECT_ROOT_PATH, 'src/webpack/entrypoint.js')
        ]
        // 'install-vendor' bundle is create by CommonsChunkPlugin
    },

    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM',
    },

    output: {
        chunkFilename: 'assets/[name].js',
        filename: '[name].js',
        path: path.resolve(PROJECT_ROOT_PATH, 'target', 'dev'),
        publicPath: `/assets/`
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {test: /[\\/]node_modules[\\/]/, name: "vendor", chunks: "all"}
            }
        },
        runtimeChunk: { name: 'assets/manifest' }
    },

    devServer: {
        contentBase: path.resolve(PROJECT_ROOT_PATH, 'target', 'dev'),
        clientLogLevel: 'warning',
        hot: true,
        historyApiFallback: true,
        port: 31080,
        publicPath: `/assets/`,
        watchContentBase: true,
        headers : {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, CONNECT, OPTIONS, TRACE',
            'Access-Control-Allow-Headers': 'Origin, X-Agent-Request'
        }
    },

    devtool: 'inline-source-map',

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                include: [
                    path.resolve(PROJECT_ROOT_PATH, 'src', 'main', 'javascript'),
                ]
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            config: {
                                path: __dirname + '/postcss.config.js'
                            }
                        },
                    },
                    'sass-loader',
                ],
            },
            { test: /\.(png|jpg)$/, use: 'url-loader?limit=15000' },
            { test: /\.eot(\?v=\d+.\d+.\d+)?$/, use: 'file-loader' },
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: 'url-loader?limit=10000&mimetype=application/font-woff' },
            { test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/, use: 'url-loader?limit=10000&mimetype=application/octet-stream' },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: 'url-loader?limit=10000&mimetype=image/svg+xml' }
        ],
    },
    plugins : [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename:  '[name].css',
            chunkFilename: 'assets/[name].css',
        }),

        new webpack.HotModuleReplacementPlugin(),

        new CopyWebpackPlugin(
            [
                {
                    from: path.resolve(PROJECT_ROOT_PATH, 'src', 'main', 'html'),
                    to: path.join(PROJECT_ROOT_PATH, 'target', 'dev'),
                    force: true
                },
            ],
            { debug: true, copyUnmodified: true }
        ),
        new WriteFilePlugin()
    ],

    bail: true
};
