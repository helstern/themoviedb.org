export function navigateTo(history, url)
{
    if (history.pushState) {
        history.pushState(null, null, url);
    }

    history.push(url);
    history.go(1)
}

/**
 * @param {object} history
 * @returns {function}
 */
export function createNavigator(history)
{
  /**
   * @param {string} url
   */
  function navigateTo(url) {
    if (history.pushState) {
      history.pushState(null, null, url);
      return;
    }

    history.push(url);
    history.go(1)
  }

    return navigateTo;
}