import React from "react";

const appContext = React.createContext();

/**
 * @param {function} component
 * @returns {function(object): *}
 */
export function consume(component)
{
    function consumer(props)
    {
        return (
            <appContext.Consumer>
                {contextProps => component(contextProps, props)}
            </appContext.Consumer>
        );
    }

    return consumer;
}

export default appContext;