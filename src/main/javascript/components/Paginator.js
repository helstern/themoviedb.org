import React from "react";
import PropTypes from 'prop-types';

export default class Paginator extends React.PureComponent
{
  static propTypes = {
    page:          PropTypes.object,
    totalPages:    PropTypes.object,
    loadPage:      PropTypes.func.isRequired
  };

  loadPrevious =() =>
  {
    this.props.loadPage(this.props.page - 1);
  };

  loadNext =() =>
  {
    this.props.loadPage(this.props.page + 1);
  };

  renderLoadPrevious()
  {
    if (this.props.page > 1) {
      return (
        <a className={"button pagination-previous"} onClick={this.loadPrevious}>
          <span className={"icon is-small"}>
            <i className="fas fa-chevron-circle-left"/>
          </span>
        </a>
      );
    }
  }

  renderLoadNext()
  {
    if (this.props.totalPages > 1 && this.props.page < this.props.totalPages) {
      return (
        <a className={"button pagination-next"} onClick={this.loadNext}>
          <span className={"icon is-small"}>
            <i className="fas fa-chevron-circle-right"/>
          </span>
        </a>
      );
    }
  }

  render()
  {
    return (
      <nav className="pagination" role="navigation" aria-label="pagination">
        <span className={"pagination-list button is-white"}>
          <span className={"label"}>Current page</span> {this.props.page} / {this.props.totalPages}
        </span>
        {this.renderLoadPrevious()}
        {this.renderLoadNext()}
    </nav>);
  }
}