import React from "react";
import PropTypes from 'prop-types';
import { authentication as authenticationLang} from "../app/languages";

export class PageAuthenticate extends React.PureComponent
{
  static propTypes = {
    errorType: PropTypes.string,
    authenticate: PropTypes.func.isRequired,
    apiKey: PropTypes.string
  };

  static defaultProps = {
    apiKey: ""
  };

  authenticate()
  {
    const apiKey = this.state && this.state.apiKey ? this.state.apiKey : this.props.apiKey;
    this.props.authenticate(apiKey);
  }

  /**
   * @param {SyntheticEvent} e
   */
  onAuthcInputKeyPress = (e) =>
  {
    if(e.key === 'Enter'){
      this.authenticate();
    }
  };

  /**
   * @param {SyntheticEvent} e
   */
  onAuthcClick = (e) => this.authenticate();

  /**
   * @param {SyntheticEvent} e
   */
  onAuthcInputChange = (e) => {
    this.setState({ apiKey: e.target.value })
  };

  renderErrorMessage()
  {
    return (
      <div className="notification is-danger">
        {authenticationLang.authenticationError}
      </div>
    )
  }

  render()
  {
    return (
      <section className={"section"}>
        <div className={"container"}>

          <h1 className={"title"}>Authentication required</h1>

          <h2 className={"subtitle"}>Write your api key in the input box below and press enter</h2>

          <div className={"authenticationbox"}>
            <div className={"searchbox-control is-left"}>
              <input type="text" className={"input is-large"} placeholder={'Your API key here'} onChange={this.onAuthcInputChange} onKeyPress={this.onAuthcInputKeyPress}/>
            </div>
            <a className={"button is-large is-light"} onClick={this.onAuthcClick}>
              {authenticationLang.authenticateButton}
            </a>
          </div>

          {this.props.errorType && this.renderErrorMessage()}

        </div>
      </section>

    );

  }
}