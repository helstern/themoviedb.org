import React from "react";

import { consume } from '../app/context';
import { selectCurrentPage } from "../app/state";
import { renderCollection, renderCompany, renderKeyword } from './Link'
import { renderMovie, renderTVShow } from './CardMediaSummary'
import { renderPerson } from './CardPersonSummary'
import SearchResultsList from "../components/SearchResultsList"

const itemRenderers = {
  people:       renderPerson,
  movies:       renderMovie,
  companies:    renderCompany,
  tv:           renderTVShow,
  collections:  renderCollection,
  keywords:     renderKeyword
};

/**
 * @param state
 * @param actions
 * @param className
 * @param searchType
 * @returns {{className: *, pageNumber: Number, totalPages: Number, searchType: *, listStyle: *, renderItem: *, items: Array}}
 */
function context({ state, actions }, { className, searchType })
{
  const renderItem = itemRenderers[searchType];

  const { pageNumber, totalPages, records } = selectCurrentPage(state, searchType);
  const { showResultPage } = actions;
  return { showResultPage, className, pageNumber, totalPages, searchType, renderItem, items: records }
}

/**
 * @param {Object} contextProps
 * @param {Object} otherProps
 * @returns {*}
 */
function render(contextProps, otherProps)
{
  const props = context(contextProps, otherProps);
  return (<SearchResultsList {...props}/>);
}
export default consume(render);