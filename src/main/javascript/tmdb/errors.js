export const TYPE_UNAUTHORIZED = 'unauthorized';

/**
 * @param response
 * @returns {Error}
 */
export function createUnauthorizedError(response)
{
  const error = new Error('unauthorized');
  error.response = response;
  error.type = TYPE_UNAUTHORIZED;

  return error;
}

/**
 * @param {Error|*} e
 * @returns {string|null}
 */
export function getErrorType(e)
{
  if (isUnauthorizedError(e)) {
    return TYPE_UNAUTHORIZED;
  }
  return null;
}

/**
 * @param {Error|*} e
 * @returns {boolean}
 */
export function isUnauthorizedError(e)
{
  return e instanceof Error && e.type === TYPE_UNAUTHORIZED;
}