/**
 * @param {...*} obj
 * @return {function(=*): Object}
 */
export function bindObjectKeys(obj)
{
  /**
   * @param {...*} args
   * @return {Object}
   */
  function bind(...args)
  {
    function reducer(acc, key)
    {
      acc[key] = obj[key].apply(null, args);
      return acc;
    }

    return Object.keys(obj).reduce(reducer, {})
  }

  return bind;
}


// export function tryAtMost(otherArgs, maxRetries, promise) {
//     promise = promise||new Promise();
//
//     // try doing the important thing
//
//     if(success) {
//         promise.resolve(result);
//     } else if (maxRetries > 0) {
//         // Try again if we haven't reached maxRetries yet
//         setTimeout(function() {
//             tryAtMost(otherArgs, maxRetries - 1, promise);
//         }, retryInterval);
//     } else {
//         promise.reject(error);
//     }
// }