import React from "react";

import { consume } from '../app/context';
import SearchBox from "../components/SearchBox"

/**
 * @param {Context} context
 * @param otherProps
 * @returns {{}}
 */
function context(context, otherProps)
{
  const { query } = context.state;
  const { search } = context.actions;

  return { query, search };
}

/**
 * @param {Object} contextProps
 * @param {Object} otherProps
 * @returns {*}
 */
function render(contextProps, otherProps)
{
  const props = context(contextProps, otherProps);
  return (<SearchBox {...props}/>);
}
export default consume(render);