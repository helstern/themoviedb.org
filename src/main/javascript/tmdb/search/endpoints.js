import {bindObjectKeys} from '../../helpers'

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(string, number=1): Promise<Response>}
 */
export function searchCompanies(client)
{
    /**
     * @param {string} query
     * @param {number} page
     * @returns {Promise<Response>}
     */
    function requestSearchCompanies(query, page = 1)
    {
        return client(`search/company?query=${query}&page=${page}`, {
                method: "GET"
            }
        );
    }

    return requestSearchCompanies;
}

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(string, number=1): Promise<Response>}
 */
export function searchCollections(client)
{
    /**
     * @param {string} query
     * @param {number} page
     * @returns {Promise<Response>}
     */
    function requestSearchCollections(query, page = 1)
    {
        return client(`search/collection?query=${query}&page=${page}`, {
                method: "GET"
            }
        );
    }

    return requestSearchCollections;
}

export function searchKeywords(client)
{
    /**
     * @param {string} query
     * @param {number} page
     * @returns {Promise<Response>}
     */
    function requestSearchKeywords(query, page = 1)
    {
        return client(`search/keyword?query=${query}&page=${page}`, {
                method: "GET"
            }
        );
    }

    return requestSearchKeywords;
}

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(string, number=1): Promise<Response>}
 */
export function searchMovies(client)
{
    /**
     * @param {string} query
     * @param {number} page
     * @returns {Promise<Response>}
     */
    function requestSearchMovies(query, page = 1)
    {
        return client(`search/movie?query=${query}&page=${page}`, {
                method: "GET"
            }
        );
    }

    return requestSearchMovies;
}

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(string, number=1): Promise<Response>}
 */
export function searchPeople(client)
{
    /**
     * @param {string} query
     * @param {number} page
     * @returns {Promise<Response>}
     */
    function requestSearchPeople(query, page = 1)
    {
        return client(`search/person?query=${query}&page=${page}`, {
                method: "GET"
            }
        );
    }

    return requestSearchPeople;
}

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(string, number=1): Promise<Response>}
 */
export function searchTVShows(client)
{
    /**
     * @param {string} query
     * @param {number} page
     * @returns {Promise<Response>}
     */
    function requestSearchTVShows(query, page = 1)
    {
        return client(`search/tv?query=${query}&page=${page}`, {
                method: "GET"
            }
        );
    }

    return requestSearchTVShows;
}

/**
 * @typedef {Object} ResourceSearch
 * @property {function(string, number)} searchCompanies
 * @property {function(string, number)} searchCollections
 * @property {function(string, number)} searchKeywords
 * @property {function(string, number)} searchMovies
 * @property {function(string, number)} searchPeople
 * @property {function(string, number)} searchTVShows
 */

/**
 * @type {function(=*): ResourceSearch}
 */
const createSearch = bindObjectKeys({
    searchCompanies,
    searchCollections,
    searchKeywords,
    searchMovies,
    searchPeople,
    searchTVShows
});

export default createSearch;
