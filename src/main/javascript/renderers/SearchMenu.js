import React from "react";

import { consume } from '../app/context';
import SearchMenu from "../components/SearchMenu"
import { selectSummaries } from "../app/state";

function context({ state, actions, navigator }, { className })
{
  const records = selectSummaries(state);
  const { showResultPage } = actions;
  return { className, navigator, showResultPage, records }
}

/**
 * @param {Object} contextProps
 * @param {Object} otherProps
 * @returns {*}
 */
function render(contextProps, otherProps)
{
  const props = context(contextProps, otherProps);
  return (<SearchMenu {...props}/>);
}

export default consume(render);