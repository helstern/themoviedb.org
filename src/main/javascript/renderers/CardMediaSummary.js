import React from "react";
import CardMediaSummary from "../components/CardMediaSummary";


/**
 * @param {Object} movie
 * @returns {{title: *, posterUrl: *, overview: *, releaseDate: *, popularity: *, detailsUrl: *}}
 */
function propsMovie(movie)
{
  const {
    id,
    title,
    poster_path: posterPath,
    overview,
    release_date: releaseDate,
    popularity,
    vote_count: voteCount,
    vote_average: voteAverage
  } = movie;

  return {
    id,
    title,
    posterUrl: posterPath ? `https://image.tmdb.org/t/p/w185/${posterPath}` : null,
    overview,
    releaseDate,
    popularity,
    voteCount,
    voteAverage,
    detailsUrl: `https://www.themoviedb.org/movie/${id}`
  };
}

/**
 * @param {Object} data
 * @returns {*}
 */
export function renderMovie(data)
{
  const props = propsMovie(data);
  return (<CardMediaSummary {...props}/>);
}

/**
 * @param {Object} data
 * @returns {*}
 */
export function renderTVShow(data)
{
  const props = propsMovie(data);
  return (<CardMediaSummary {...props}/>);
}