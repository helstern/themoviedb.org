import {bindObjectKeys} from '../../helpers'

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(String): Promise<Response>}
 */
export function getDetails(client)
{
    /**
     * @param {String} id
     * @returns {Promise<Response>}
     */
    function requestGetDetails(id)
    {
        return client(`keyword/${id}`, { method: "GET" });
    }

    return requestGetDetails;
}

/**
 * @typedef {Object} ResourceKeywords
 * @property {function} getDetails
 */

/**
 * @type {function(=*): ResourceKeywords}
 */
const createKeywords = bindObjectKeys({
    getDetails
});

export default createKeywords;
