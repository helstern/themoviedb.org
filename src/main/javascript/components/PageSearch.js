import React from "react";
import PropTypes from 'prop-types';

class PageSearch extends React.PureComponent
{
    static propTypes = {
        searchType:           PropTypes.string.isRequired,
        renderSearchBox:      PropTypes.func.isRequired,
        renderSearchMenu:     PropTypes.func.isRequired,
        renderSearchResults:  PropTypes.func.isRequired
    };

    render()
    {
        return(
            <div className={"container"}>
                <header>
                  {this.props.renderSearchBox()}
                </header>

                <main className={"columns"}>
                  {this.props.renderSearchMenu({
                    className: "column"
                  })}

                  {this.props.renderSearchResults({
                    className: "column is-four-fifths",
                    searchType: this.props.searchType
                  })}
                </main>
            </div>
        );

    }
}

export default PageSearch;