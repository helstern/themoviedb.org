export const searchTypes = [
    'movies',
    'people',
    'companies',
    'tv',
    'collections',
    'keywords'
];
