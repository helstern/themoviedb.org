import {bindObjectKeys} from '../../helpers'

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(String): Promise<Response>}
 */
export function getDetails(client)
{
    /**
     * @param {String} id
     * @returns {Promise<Response>}
     */
    function requestGetDetails(id)
    {
        return client(`collection/${id}`, { method: "GET" });
    }

    return requestGetDetails;
}

/**
 * @typedef {Object} ResourceCollections
 * @property {function} getDetails
 */

/**
 * @type {function(=*): ResourceCollections}
 */
const createCollections = bindObjectKeys({
    getDetails
});

export default createCollections;
