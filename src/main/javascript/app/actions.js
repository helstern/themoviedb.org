import { bindObjectKeys } from '../helpers';
import { getErrorType } from '../tmdb/errors';
import { searchTypes } from './configuration';
import { selectResultPage } from './state';

const ACTION_SEARCH = 'search';
const ACTION_SHOW_RESULT_PAGE = 'show-result-page';
const ACTION_SHOW_DETAILS = 'show-details';
const ACTION_AUTHENTICATE = 'authenticate';

/**
 * @param {API} api
 * @returns {{companies: ((function(string, number))|searchCompanies), collections: ((function(string, number))|searchCollections), keywords: ((function(string, number))|searchKeywords), movies: ((function(string, number))|searchMovies), people: ((function(string, number))|searchPeople), tv: ((function(string, number))|searchTVShows)}}
 */
function searchEndpointsByType(api)
{
    return {
        'companies':    api.search.searchCompanies,
        'collections':  api.search.searchCollections,
        'keywords':     api.search.searchKeywords,
        'movies':       api.search.searchMovies,
        'people':       api.search.searchPeople,
        'tv' :          api.search.searchTVShows
    };
}

/**
 * @param {object} state
 * @param {object} action
 */
export default function reducer(state, action)
{
  if (ACTION_AUTHENTICATE === action.type) {
    const { configuration } = action;
    return { ...state, configuration };
  }

    if (ACTION_SHOW_RESULT_PAGE === action.type) {

        const { resultsType, query } = action;
        const { results } = state;

        const nextResults = { ...results };
        nextResults[resultsType] = {
            pageNumber: query.page,
            totalPages: query.total_pages,
            records: query.results
        };

        return { ...state, results: nextResults };
    }

    if (ACTION_SEARCH === action.type) {

        const { queries, query } = action;

        const reducer = function(acc, type) {
            const query = queries[type];

            acc.summaries[type] = {
                total: query.total_results
            };

            acc.results[type] = {
                pageNumber: query.page,
                totalPages: query.total_pages,
                records: query.results
            };

            return acc;
        };

        const { summaries, results } =  searchTypes.reduce(reducer, { summaries : {}, results: {} });
        return { ...state, summaries, results, query, display: 'movies' };
    }

    if (ACTION_SHOW_DETAILS === action.type) {
        const { details } = action;
        return { ...state, details };
    }

    return state;
}

/**
 * @param {API} api
 * @param {function} navigator
 * @param {function(object)} dispatch
 * @returns {function(*=): Promise<any>}
 */
function search({api, navigator, dispatch})
{
    const endpoints = searchEndpointsByType(api);

    function execQuery(query, type, queries)
    {
      const endpoint = endpoints[type];
      return endpoint(query, 1).then(response => response.json()).then(body => queries[type] = body);
    }

    function actionSearch(query) {

        const queries = {};

        const promises = searchTypes.map(type => execQuery(query, type, queries));

        Promise.all(promises).then(() => {
            dispatch({ type: ACTION_SEARCH, queries, query });
            navigator('/search/movies');
            return queries;
        });

        return Promise.resolve(query);
    }

    return actionSearch;
}

/**
 * @param {API} api
 * @param {function} navigator
 * @param {function(object)} dispatch
 * @param {function} getState
 * @returns {function(string, string, string)}
 */
function showResultPage({api, navigator, dispatch, getState})
{
    const endpoints = searchEndpointsByType(api);

    function execQuery(endpoint, query, page) {
      if (typeof endpoint !== "function") {
       return Promise.reject(new Error('unknown type'));
      }

      return endpoint(query, page)
        .then(response => {
          const body = response.json();

          if (response.status === 200) {
            return body;
          }

          return Promise.reject(body);
        })
      ;
    }

    function actionShowResultPage(type, page) {

        const state = getState();
        const resultPage = selectResultPage(state, type, page);

        let queryPromise;
        if (resultPage) {
          queryPromise = Promise.resolve(resultPage);
        } else {
          const { query } = state;
          const endpoint = endpoints[type];
          queryPromise = execQuery(endpoint, query, page).then(results => {
            dispatch({
              type:           ACTION_SHOW_RESULT_PAGE,
              resultsType:    type,
              query:          results
            });
            return results;
          });
        }

        return queryPromise.then(() => navigator(`/search/${type}`));
    }

    return actionShowResultPage;
}

/**
 * @param {API} api
 * @param {function} authenticate
 * @param {function} navigator
 * @param {function(object)} dispatch
 * @param {function} getState
 * @return {function(string, string)}
 */
function authenticate({api, authenticate, navigator, dispatch, getState})
{
  function actionAuthenticate (apiKey)
  {
    return authenticate(apiKey)
      .then(response => response.json())
      .then(configuration => {
        dispatch({ type: ACTION_AUTHENTICATE, configuration });
        navigator('/');
        return configuration;
      })
      .catch(e => {
        const errorType = getErrorType(e) || 'authentication-failure';
        navigator(`authenticate?error=${errorType}`)
      })
      ;
  }

  return actionAuthenticate;
}

/**
 * @typedef {object} Actions
 * @property {function(string)} search
 * @property {function(string, string)} showDetails
 * @property {function(string, string)} showResultPage
 * @property {function(string)} authenticate
 */

/**
 * @type {function({api:API, authenticate: function, navigator:function, dispatch: function(object), getState: function }): Actions}
 */
export const createActions = bindObjectKeys({
    search,
    showResultPage,
    authenticate
});
