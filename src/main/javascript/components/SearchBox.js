import React from "react";
import PropTypes from 'prop-types';

import { search as searchLang } from "../app/languages";

export default class SearchBox extends React.Component
{
    static propTypes = {
        query: PropTypes.string,
        search: PropTypes.func.isRequired
    };

    static defaultProps = {
        query: ""
    };

  search()
  {
    const query = this.state && this.state.query ? this.state.query : this.props.query;
    this.props.search(query);
  }

  /**
   * @param {SyntheticEvent} e
   */
    onQueryInputKeyPress = (e) => {
      if(e.key === 'Enter'){
        this.search();
      }
    };

    onSearchClick = (e) => this.search();

  /**
   * @param {SyntheticEvent} e
   */
    onQueryInputChange = (e) => {
      this.setState({ query: e.target.value })
    };

    render()
    {
        const query = this.state && this.state.query ? this.state.query : this.props.query;

        return (
            <div className={"searchbox"}>
                <div className={"searchbox-control has-icons-left is-left"}>
                    <input type="text" className={"input is-large"} value={query} placeholder={searchLang.searchPlaceholder} onChange={this.onQueryInputChange} onKeyPress={this.onQueryInputKeyPress}/>
                    <span className={"icon is-medium is-left"}>
                        <i className={"fas fa-search"}/>
                    </span>
                </div>
                <a className={"button is-large is-light"} onClick={this.onSearchClick}>
                    {searchLang.searchButton}
                </a>
            </div>
        );
    }
}

