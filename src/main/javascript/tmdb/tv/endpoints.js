import {bindObjectKeys} from '../../helpers'

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(String): Promise<Response>}
 */
export function getDetails(client)
{
    /**
     * @param {String} id
     * @returns {Promise<Response>}
     */
    function requestGetDetails(id)
    {
        return client(`tv/${id}`, { method: "GET" });
    }

    return requestGetDetails;
}

/**
 * @typedef {Object} ResourceTV
 * @property {function} getDetails
 */

/**
 * @type {function(=*): ResourceTV}
 */
const createTv = bindObjectKeys({
    getDetails
});

export default createTv;