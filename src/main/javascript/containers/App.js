import React from "react";
import PropTypes from "prop-types";

import { Route, Router, Redirect } from 'react-router'
import queryString from 'query-string';

import PageHome from '../components/PageHome';
import PageSearch from '../components/PageSearch';
import {PageAuthenticate} from "../components/PageAuthenticate";
import context from '../app/context';
import { selectConfiguration } from '../app/state';
import renderSearchBox from '../renderers/SearchBox'
import renderSearchResults from '../renderers/SearchResults'
import renderSearchMenu from '../renderers/SearchMenu'


function createAuthenticator(checkAuthenticated, renderer)
{
  function authenticate(props)
  {
    if (checkAuthenticated()) {
      return renderer(props);
    }

    return (<Redirect to='/authenticate' />);
  }

  return authenticate;
}

export class App extends React.Component
{
    static propTypes = {
        actions: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
        subscribe: PropTypes.func.isRequired
    };

    state = {
        appState: null
    };

    componentDidMount()
    {
        this.props.subscribe(this.onStateChange);
    }

    onStateChange = (getState) =>
    {
        this.setState({appState: getState()})
    };

    checkAuthenticated = () => {
      const { appState } = this.state;
      const configuration = selectConfiguration(appState);
      return !!configuration;
    };

    /**
     * @typedef {Object} Context
     * @property {object} state
     * @property {function} actions
     */

    /**
     * @returns {Context}
     */
    getContext()
    {
      return {
        state:    this.state.appState,
        actions:  this.props.actions
      };
    }

    renderPageSearch = createAuthenticator(this.checkAuthenticated, (props) =>
    {
      const { match } = props;
      const searchType = match.params.searchType || 'movies';

      return (<PageSearch {...props}
        searchType=         {searchType}
        renderSearchBox=    {renderSearchBox}
        renderSearchResults={renderSearchResults}
        renderSearchMenu=   {renderSearchMenu}
      />);
    });

    renderPageAuthenticate = (props) =>
    {
      const { location } = props;
      const query = queryString.parse(location.search);

      return (<PageAuthenticate {...props}
        errorType={query.error}
        authenticate={this.props.actions.authenticate}
      />);
    };

    renderPageHome = createAuthenticator(this.checkAuthenticated,(props) => {
      return (<PageHome {...props}
        renderSearchBox=  {renderSearchBox}
      />);
    });

    render()
    {
        if (this.state.appState) {
          return(
            <context.Provider value={this.getContext()}>
              <Router history={this.props.history} >
                <section className={"section"}>
                  <Route exact path="/" render={this.renderPageHome} />
                  <Route exact path="/search/:searchType" render={this.renderPageSearch} />
                  <Route exact path="/authenticate" render={this.renderPageAuthenticate} />
                </section>
              </Router>
            </context.Provider>
          );
        }

        return null;
    }
}
