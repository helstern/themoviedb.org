import createClient from "../../../main/javascript/tmdb/client";
import { isUnauthorizedError } from "../../../main/javascript/tmdb/errors";

const actualFetch = global.fetch;

function createResponse(body, options)
{
  return new window.Response(JSON.stringify(body), options);
}

function fetchEcho (url, opts) {

  const response = createResponse({url, opts}, {
    status: 200,
    statusText: 'Ok',
    headers: {
      'Content-type': 'application/json'
    }
  });

  return Promise.resolve(response);
}

describe('client tests', () => {

  it('rejects with authorization error', () => {

    const theFetch = global.fetch;
    global.fetch = function (url, opts) {

      const response = createResponse({}, {
        status: 401,
        statusText: 'Unauthorized',
        headers: {
          'Content-type': 'application/json'
        }
      });

      return Promise.resolve(response);
    };

    const { client } = createClient({ apiKey: 'key', apiHost: 'localhost', apiVersion: '3' });
    return client('/myresource', {})
      .then(() => Promise.reject('client should reject'))
      .catch(e => {
        return expect(isUnauthorizedError(e)).toBe(true);
    });
  });

  it('forms the request url when query params are present', () => {
    global.fetch = fetchEcho;

    const { client } = createClient({ apiKey: 'key', apiHost: 'localhost', apiVersion: '3' });
    return client('my/endpoint?param=1', {})
      .then(response => response.json())
      .then(({url}) => expect(url).toBe('https://localhost/3/my/endpoint?param=1&api_key=key'))
    ;
  });

  it('forms the request url', () => {
    global.fetch = fetchEcho;

    const { client } = createClient({ apiKey: 'key', apiHost: 'localhost', apiVersion: '3' });
    return client('my/endpoint', {})
      .then(response => response.json())
      .then(({url}) => expect(url).toBe('https://localhost/3/my/endpoint?api_key=key'))
    ;
  });

});

