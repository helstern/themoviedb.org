import React from "react";
import CardPersonSummary from "../components/CardPersonSummary";
import { renderMovie } from './Link'

function propsPerson(movie)
{
  const { name, profile_path: profilePath, id, known_for: knownFor } = movie;

  return {
    id,
    name,
    profileUrl: profilePath ? `https://image.tmdb.org/t/p/w185/${profilePath}` : null,
    knownFor,
    renderKnownFor: renderMovie,
    detailsUrl: `https://www.themoviedb.org/person/${id}`
  };
}

export function renderPerson(data)
{
  const props = propsPerson(data);
  return (<CardPersonSummary {...props}/>);
}