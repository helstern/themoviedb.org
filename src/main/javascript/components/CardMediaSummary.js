import React from "react";
import PropTypes from 'prop-types';

export default class CardMediaSummary extends React.PureComponent
{
  static propTypes = {
      posterUrl:    PropTypes.string.isRequired,
      title:        PropTypes.string.isRequired,
      overview:     PropTypes.string.isRequired,
      detailsUrl:   PropTypes.string.isRequired,

      releaseDate:  PropTypes.string,
      popularity:   PropTypes.string,
      voteCount:    PropTypes.string,
  };

  static defaultProps = {
    releaseDate: "N/A",
    popularity: "N/A",
    voteCount: "N/A"
  };
  renderPosterNormal()
  {
    return (
      <img
        alt={this.props.title}
        src={this.props.posterUrl}
      />
    );
  }

  renderPosterEmpty()
  {
    return (
      <div className={"photoPlaceholder"}>
        <i className="fas fa-image fa-5x photoPlaceholder-background"/>
      </div>
    )
  }

    render()
    {
        return (
          <div className="mediaCard">

            <figure className="mediaCard-media">
              <a className="image"
                 href=  {this.props.detailsUrl}
                 target={'blank'}
                 title= {this.props.title}
                 alt=   {this.props.title}
              >
                { this.props.posterUrl ? this.renderPosterNormal() : this.renderPosterEmpty() }

              </a>
            </figure>

            <section className="mediaCard-content">
              <div className={"content"}>
                <h1>
                  <span>{this.props.title}</span>
                </h1>

                <p className={"mediaCard-year"}>
                  <span className={"label"}>Release date</span>
                  {this.props.releaseDate}
                </p>

                <p className={"mediaCard-overview"}>{this.props.overview}</p>

                <footer className={"mediaCard-footer"}>

                  <div className="control">
                    <div className="tags has-addons">
                      <span className="tag">Vote count</span>
                      <span className="tag is-info">{this.props.voteCount || 'N/A'}</span>
                    </div>
                  </div>

                  <div className="control">
                    <div className="tags has-addons">
                      <span className="tag">Popularity</span>
                      <span className="tag is-info">{this.props.popularity || 'N/A'}</span>
                    </div>
                  </div>

                </footer>
              </div>
            </section>

          </div>
        )
    }
}