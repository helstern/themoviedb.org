import queryString from 'query-string';

import '../polyfills'
import '../main/sass/index.scss'
import run from '../main/javascript/index'

window.addEventListener('load', function () {
    const config = queryString.parse(window.location.search);
    run(config, window)
});
