import React from "react";
import { createMemoryHistory, createBrowserHistory } from 'history'

import { App } from "../containers/App";
import state from './state'
import { default as reducer, createActions } from './actions'
import { createStore } from './store'
import { createClient, createApi } from '../tmdb'
import { createNavigator } from './navigation'

/**
 * @param {String} [apiKey]
 * @returns {function}
 */
export default function bootstrap({ apiKey })
{
    // const history = createMemoryHistory({ initialEntries: ['/search'], initialIndex: 0});
    const history = createBrowserHistory({
        forceRefresh: false,
        keyLength: 6
    });
    const initialState = state();
    const { getState, dispatch, subscribe } = createStore(initialState, reducer);
    const { client, authenticate } = createClient({ apiKey });
    const api = createApi(client);
    const navigator = createNavigator(history);
    const actions = createActions({ api, navigator, authenticate, dispatch, getState});


    return class extends React.PureComponent
    {
        render()
        {
            return <App actions={actions} subscribe={subscribe} history={history}/>
        }
    };
}