import {bindObjectKeys} from '../../helpers'

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(String): Promise<Response>}
 */
export function getDetails(client)
{
    /**
     * @param {String} id
     * @returns {Promise<Response>}
     */
    function requestGetDetails(id)
    {
        return client(`company/${id}`, { method: "GET" });
    }

    return requestGetDetails;
}

/**
 * @typedef {Object} ResourceCompanies
 * @property {function} getDetails
 */

/**
 * @type {function(=*): ResourceCompanies}
 */
const createCompanies = bindObjectKeys({
    getDetails
});

export default createCompanies;