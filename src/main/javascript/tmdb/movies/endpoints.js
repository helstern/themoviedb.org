import {bindObjectKeys} from '../../helpers'

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(String): Promise<Response>}
 */
function getDetails(client)
{
    /**
     * @param {String} id
     * @returns {Promise<Response>}
     */
    function requestGetDetails(id)
    {
        return client(`movie/${id}`, { method: "GET" });
    }

    return requestGetDetails;
}

/**
 * @typedef {Object} ResourceMovies
 * @property {function} getDetails
 */

/**
 * @type {function(=*): ResourceMovies}
 */
const createMovies = bindObjectKeys({
    getDetails
});

export default createMovies;