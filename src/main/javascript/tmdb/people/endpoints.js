import {bindObjectKeys} from '../../helpers'

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(String): Promise<Response>}
 */
export function getDetails(client)
{
    /**
     * @param {String} id
     * @returns {Promise<Response>}
     */
    function requestGetDetails(id)
    {
        return client(`person/${id}`, { method: "GET" });
    }

    return requestGetDetails;
}

/**
 * @typedef {Object} ResourcePeople
 * @property {function} getDetails
 */

/**
 * @type {function(=*): ResourcePeople}
 */
const createPeople = bindObjectKeys({
    getDetails
});

export default createPeople;