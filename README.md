# themoviedb.org

This project searches themoviedb.org for movies, people, companies, tv shows, collectons and keywords and displays the results

## Requirements

This project was developed and tested using nodejs `v8.9.4` and npm `v5.6.0` on **Linux**. It should be fine to use other versions of node and npm, but the recommendation is to use compatible versions.
 
## Installation

Clone and run the following command in the root of the project:

    npm install

## Run the project

From the root of the project run:
    
    npm run dev
    
Open [http://localhost:31080](http://localhost:31080) in your browser. You wil need a valid api key from [themoviedb.org](https://www.themoviedb.org/settings/api).

## Test

From the root of the project run:
    
    npm run test        
    
## Todo

 - the click handlers for search, pagination etc are not throttled
 - components like search page, buttons with actions do not have a loading state    