import { createUnauthorizedError} from './errors';
const defaultApiHost = 'api.themoviedb.org';
const defaultApiVersion = '3';


/**
 * @param {String} apiKey
 * @param {String} [apiHost]
 * @param {String} [apiVersion]
 * @returns {{ client, authenticate }}
 */
export default function createClient({ apiKey, apiHost, apiVersion })
{
    let key = apiKey || '' ;
    const host = apiHost || defaultApiHost;
    const version = apiVersion || defaultApiVersion;

    /**
     * @param {String} resourceUrl
     * @param {Object} reqOpts
     * @returns {Promise<Response>}
     */
    function client(resourceUrl, reqOpts)
    {
        const querySeparator = -1 === resourceUrl.indexOf('?') ? '?' : '&';
        const query = `${querySeparator}api_key=${key}`;

        const apiUrl = `https://${host}/${version}/${resourceUrl}${query}`;
        return fetch(apiUrl, reqOpts).then(response => {
            if (response.status === 401) {
              return Promise.reject(createUnauthorizedError(response));
            }
            return response;
        });
    }

  /**
   * @param apiKey
   * @returns {Promise<Response>}
   */
  function authenticate(apiKey)
    {
        let oldKey = key;
        key = apiKey;

        return client(`configuration`, { method: "GET" }).catch(e => {
            key = oldKey;
            return Promise.reject(e);
        });
    }

    return { client, authenticate };
}
