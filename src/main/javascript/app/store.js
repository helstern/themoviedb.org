/**
 * @param {function():object} getState
 * @param {function(object)} setState
 * @param {function(function)} notify
 * @param {function(object, object)} reducer
 * @returns {(function(object))}
 */
function createDispatch(getState, setState, notify, reducer)
{
    /**
     * @param {object} action
     */
    function dispatch(action) {

        const state = getState();
        const reducedState = reducer(state, action);

        if (reducedState !== state) {
            setState({ ...state, ...reducedState });
            notify(getState)
        }
    }

    return dispatch;
}

/**
 * @param initialState
 * @returns {{setState: (function(object)), getState: (function(): any)}}
 */
function state(initialState)
{
    let state = initialState;

    /**
     * @returns {any}
     */
    function getState()
    {
        return JSON.parse(JSON.stringify(state));
    }

    /**
     * @param {object} newState
     */
    function setState(newState)
    {
        state = newState;
    }

    return { setState, getState };
}

/**
 * @param {function} getState
 * @returns {{notify: (function(*)), subscribe: (function(function))}}
 */
function listeners(getState)
{
    const subscribers = [];

    /**
     * @param {function} handler
     */
    function subscribe(handler)
    {
        subscribers.push(handler);
        handler(getState)
    }

    /**
     * @param {...any} args
     */
    function notify(...args)
    {
        subscribers.map(handler => handler.apply(null, args));
    }

    return { notify, subscribe };
}

/**
 * @param {object} initialState
 * @param {function(object, object)} reducer
 * @returns {{dispatch: (function(object)), getState: (function(): any), subscribe: (function(Function))}}
 */
export function createStore(initialState, reducer)
{
    const { setState, getState } = state(initialState);

    const { subscribe, notify } = listeners(getState);

    return {

        dispatch: createDispatch(getState, setState, notify, reducer),

        getState,

        subscribe
    }
}