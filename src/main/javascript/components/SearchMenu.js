import React from "react";
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { searchTypes as searchTypesLang } from "../app/languages";

const summaryType = PropTypes.shape({
  searchType:  PropTypes.string.isRequired,
  total:    PropTypes.string.isRequired
});

export default class SearchMenu extends React.PureComponent
{
    static propTypes = {
        className       : PropTypes.string,
        records         : PropTypes.arrayOf(summaryType).isRequired,
        showResultPage  : PropTypes.func.isRequired
    };

    static defaultProps = {
        className: ""
    };

    /**
     * @param {SyntheticEvent} e
     */
    onMenuItemClick = (e) =>
    {
        const searchType = e.target.getAttribute('data-type');
        if (searchType) {
            this.props.showResultPage(searchType, 1);
        }
    };

    /**
     * @param {string} searchType
     * @param {number} total
     * @returns {*}
     */
    renderMenuItem = ({ searchType, total }) =>
    {
        return (
            <li>
                <a data-type={searchType} onClick={this.onMenuItemClick}>
                    {searchTypesLang[searchType]}
                    <span className={"searchmenu-total"}> {total}</span>
                </a>
            </li>
        );
    };

    render()
    {
        return (
            <nav className={classNames("searchmenu", this.props.className)}>
                <ul className="searchmenu-list">
                    {this.props.records.map(this.renderMenuItem)}
                </ul>
            </nav>
        );
    }
}