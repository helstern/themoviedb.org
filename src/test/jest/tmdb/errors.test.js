import { createUnauthorizedError,TYPE_UNAUTHORIZED } from "../../../main/javascript/tmdb/errors";

describe('errors test', () => {

  it('creates an unauthorized errror', () => {
      const response = {};
      const error = createUnauthorizedError(response);

      expect(error).toBeInstanceOf(Error);
      expect(error.response).toEqual(response);
      expect(error.type).toEqual(TYPE_UNAUTHORIZED);
  });

});
