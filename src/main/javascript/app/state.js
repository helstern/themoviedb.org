export default function getInitialState()
{
    return {
        query: "",

        display: "",

        configuration: null,

        summaries: {
          movies: {
            total: 0,
          },
          companies: {
            total: 0,
          },
          keywords: {
            total: 0,
          },
          collections: {
            total: 0,
          },
          people: {
            total: 0,
          },
          tv: {
            total: 0,
          }
        },

        results: {
          movies: {
            pageNumber: 1,
            totalPages: 1,
            results: []
          },
          companies: {
            pageNumber: 1,
            totalPages: 1,
            results: []
          },
          keywords: {
            pageNumber: 1,
            totalPages: 1,
            results: []
          },
          collections: {
            pageNumber: 1,
            totalPages: 1,
            results: []
          },
          people: {
            pageNumber: 1,
            totalPages: 1,
            results: []
          },
          tv: {
            pageNumber: 1,
            totalPages: 1,
            results: []
          }
        }
    };
}

/**
 * @param {Object} state
 * @returns {string}
 */
export function selectConfiguration(state)
{
  const { configuration } = state;

  return configuration;
}

/**
 * @param {Object} state
 * @returns {string}
 */
export function selectQuery(state)
{
  const { query } = state;

  return query;
}

/**
 * @param {Object} state
 * @param {string} searchType
 * @param page
 * @returns {{pageNumber: number, totalPages: Number, records:Array}[]}
 */
export function selectResultPage(state, searchType, page)
{
  const recordSet = selectCurrentPage(state, searchType);
  return recordSet && recordSet.pageNumber === page ? recordSet : null;
}

/**
 * @param {Object} state
 * @returns {{searchType: string, total: Number}[]}
 */
export function selectSummaries(state)
{
  const { summaries } = state;
  return Object.keys(summaries).map(searchType => ({ searchType, total: summaries[searchType].total }))
}

/**
 * @param {Object} state
 * @param {String} searchType
 * @returns {{pageNumber:Number, totalPages:Number, records: Array}}
 */
export function selectCurrentPage(state, searchType)
{
  const { results } = state;
  const { pageNumber, totalPages, records } =  results[searchType];

  return { pageNumber, totalPages, records };
}