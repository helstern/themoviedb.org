import React from "react";
import PropTypes from 'prop-types';
import List from './List';

export default class CardPersonSummary extends React.PureComponent
{
  static propTypes = {
    name:       PropTypes.string.isRequired,
    profileUrl: PropTypes.string.isRequired,
    detailsUrl: PropTypes.string.isRequired,

    renderKnownFor: PropTypes.func.isRequired,
    knownFor:       PropTypes.arrayOf(PropTypes.object).isRequired,
  };

  renderProfileNormal()
  {
    return (
      <img className={"profile"}
           alt={this.props.name}
           src={this.props.profileUrl}
      />
    );
  }

  renderProfileEmpty()
  {
    return (
      <div className={"photoPlaceholder"}>
        <i className="fas fa-user fa-5x photoPlaceholder-background"/>
      </div>
      )
  }

  render()
  {
    return (
      <div className="mediaCard">

        <figure className="mediaCard-media">
          <a className="image"
             href=  {this.props.detailsUrl}
             target={'blank'}
             title= {this.props.name}
             alt=   {this.props.name}
          >
            { this.props.profileUrl ? this.renderProfileNormal() : this.renderProfileEmpty() }
          </a>
        </figure>

        <section className="mediaCard-content">
          <div className={"content"}>
            <h1>
              <span>{this.props.name}</span>
            </h1>

            <p className={"mediaCard-overview"}>
              <span className={"label"}>Known for</span>
              <br/>
              <List
                listStyle={'comma-separated'}
                items={this.props.knownFor}
                renderItem={this.props.renderKnownFor}
              />
            </p>

          </div>
        </section>

      </div>
    );

  }
}