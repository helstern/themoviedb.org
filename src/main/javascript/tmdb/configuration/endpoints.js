import {bindObjectKeys} from '../../helpers'

/**
 * @param {function(String, Object): Promise<Response>} client
 * @returns {function(String): Promise<Response>}
 */
export function getAPIConfiguration(client)
{
  /**
   * @returns {Promise<Response>}
   */
  function requestGetAPIConfiguration()
  {
    return client(`configuration`, { method: "GET" });
  }

  return requestGetAPIConfiguration;
}

/**
 * @typedef {Object} ResourceConfiguration
 * @property {function} getAPIConfiguration
 */

/**
 * @type {function(=*): ResourceConfiguration}
 */
const createConfiguration = bindObjectKeys({
  getAPIConfiguration
});

export default createConfiguration;