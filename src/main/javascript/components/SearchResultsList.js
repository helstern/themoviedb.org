import React from "react";
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { searchTypes as searchTypesLang } from "../app/languages";
import List from "./List";
import Paginator from "./Paginator";

export default class SearchResultsList extends React.PureComponent
{
    static defaultProps = {
        className: ""
    };

    static propTypes = {
        className   : PropTypes.string,

        pageNumber  : PropTypes.number.isRequired,
        totalPages  : PropTypes.number.isRequired,

        searchType  : PropTypes.string.isRequired,

        renderItem  : PropTypes.func.isRequired,
        items       : PropTypes.array.isRequired,

        showResultPage: PropTypes.func.isRequired
    };

    loadPage = (page) =>
    {
        this.props.showResultPage(this.props.searchType, page);
    };

    renderEmpty()
    {
        return (
          <div className={classNames("search-results", this.props.className)}>
            <h1 className={"title"}>{searchTypesLang[this.props.searchType]}</h1>

            <p>There are no results</p>

          </div>
        );
    }

    renderNormal()
    {
      return (
        <div className={classNames("search-results", this.props.className)}>
          <h1 className={"title"}>{searchTypesLang[this.props.searchType]}</h1>

          <Paginator totalPages={this.props.totalPages} page={this.props.pageNumber} loadPage={this.loadPage} />

          <List items={this.props.items} renderItem={this.props.renderItem} />

          {this.props.totalPages > 1 && <Paginator totalPages={this.props.totalPages} page={this.props.pageNumber} loadPage={this.loadPage} /> }
        </div>
      );
    }

    render()
    {
        if (this.props.items.length === 0 && this.props.pageNumber === 1 && this.props.totalPages === 1) {
            return this.renderEmpty();
        }

        return this.renderNormal();
    }
}