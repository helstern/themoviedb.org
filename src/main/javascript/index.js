import React from "react";
import ReactDOM from "react-dom";

import bootstrap from "./app/bootstrap";

export default function run({ apiKey }, window)
{
    const App = bootstrap({});
    ReactDOM.render(<App />, window.document.body);
}
